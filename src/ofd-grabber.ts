import { Receipts, Receipt } from './@types/receipts';
import Axios, * as axios from 'axios'
import * as http from 'http'
import * as https from 'https'
import { AxiosRequestConfig, AxiosStatic } from 'axios'


Axios.defaults.httpAgent = new http.Agent({ maxSockets: 5, maxFreeSockets: 5, })
Axios.defaults.httpsAgent = new https.Agent({ maxSockets: 5, maxFreeSockets: 5, })
var invalidationTime: number = 1000 * 10 * 30; // 30 min
export class OFDgrabber {
  private uris: string[]
  public cache: Object
  


  

  constructor() { this.cache = {}}

  public grab = async (kktRegNumber: string, pDateFrom: string, pDateTo: string, token: string) => {
    let cacheKey = kktRegNumber + pDateFrom + pDateTo;
    var cache = undefined;
    if (cacheKey in this.cache) {
      cache = this.cache[cacheKey];
      if (Date.now() - cache.date > invalidationTime) {
        delete this.cache[cacheKey]
        cache = undefined
      }
    }

    if (cache !== undefined) {
      return cache.value;
    }

    let dateFrom: string = `${pDateFrom}%2000:00:00`
    let dateTo: string = `${pDateTo}%2023:59:59`
    let checks_url = `https://ofv-api-v0-1-1.evotor.ru/v1/client/${kktRegNumber}/receipts?dateFrom=${dateFrom}&dateTo=${dateTo}`

    var co: AxiosRequestConfig = { headers: { token } }
    var obody: Receipts
    try {
      let getres = (await Axios.get(checks_url, co))
      if (getres.status != 200) {
        console.error(getres)
      }
      obody = getres.data
    } catch (e) {
      console.group(`error on kktRegNumber ${kktRegNumber}`)
      console.error(e)
      console.groupEnd()
      return []
    }
    let receipts = obody.receipts;
    receipts.sort((element: Receipt, element2: Receipt) => {

      return element.requestNumber - element2.requestNumber
    });

    cache = { date: Date.now(), value: receipts }
    this.cache[cacheKey] = cache;
    console.log(Object.keys(this.cache))
    return receipts
    // request.default({
    //   headers: {
    //     token
    //   },
    //   url: checks_url,
    //   method: 'GET'
    // }, function (err, res, body) {
    //   console.log('statusCode:', res.statusCode);
    //   console.log('headers:', res.headers);
    //   console.log(body);

    //   console.log(obody)
    //   let receipts = obody.receipts
    //   receipts.sort((element: Receipt, element2: Receipt) => {

    //     return element.requestNumber - element2.requestNumber
    //   });
    //   receipts.forEach(element => {
    //     console.log(`${element.requestNumber}: ${element.totalSum}`)
    //   });



    // });
  }

}


