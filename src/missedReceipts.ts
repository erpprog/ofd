import { Receipt } from './@types/receipts';
import { MissedFromSQL, MissedItem } from './@types/missedFromSql';

/*
Делает запрос в http сервис, который выполняет SQL запрос по всем ТТ
*/
import get from 'axios'
import { query } from './query'
import { resolve } from 'dns';
import Axios from 'axios';

export class MissedReceipts {
  constructor() {
    this.url = `http://10.1.2.2:8810?SQL=${query}`


  }
  url: string
  missings: MissedItem[] = []
  public async getMissed(): Promise<MissedItem[]> {

    // console.group('getMissed')
    let body = (await get(this.url)).data
    let missings_arayed = body
    missings_arayed.forEach(e => {
      if (e.length && 'kktCode' in e[0]) {
        this.missings = this.missings.concat(e)

      }


      // console.dir(this.missings)
      // console.groupEnd()


    })
    return (this.missings)
      ;
  }


}