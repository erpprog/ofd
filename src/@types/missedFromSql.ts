// Generated by https://quicktype.io

export interface MissedFromSQL {
	receipts: MissedItem[];
}
export interface MissedItem {
  ib:          string;
  ib_href?:    string;
  kktZavNom?:  string;
  kktCode?:    string;
  host?:       string;
  outNumbers?: string;
  shift?:      number;
}
