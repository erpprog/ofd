import { config, IOptions, IResult, IRecordSet } from 'mssql';
import * as mssql from 'mssql';
import { Receipt, Receipts } from './@types/receipts';
import { MissedReceipts } from './missedReceipts';
import { OFDgrabber } from './ofd-grabber'
import * as http from 'http'

var tokens: any = getTokens()
var ofg: OFDgrabber = new OFDgrabber
const PORT = 8813;
const httpListener = http.createServer()
httpListener.listeners("get")
httpListener.listen(PORT).on("request", (req, res) => httpRequestHandler(req, res))

async function httpRequestHandler(req, res: http.ServerResponse) {
  console.log(req)
  if (req.url == '/') {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'origin, content-type, accept');
    go().then(resu => { res.end(JSON.stringify(resu)) })
  } else if (req.url == '/cache') {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.end(JSON.stringify(ofg.cache))
  }
}

async function go() {
  var result = []

  var m: MissedReceipts = new MissedReceipts()
  var misseds = await m.getMissed()
  console.table(misseds)
  misseds = misseds.filter(e => { return e.ib != '7700121' })
  
  let procs = []
  for await (const missed of misseds) {
    console.log('start ' + missed.ib)
    procs.push(processing(missed, result));


  };

  
  await Promise.all(procs)
  return result


}

async function processing(missed: import("src/@types/missedFromSql").MissedItem, result: any[]) {
  try {
    tokens = await tokens
    let token = tokens.filter(e => { return e.kkm_code == Number.parseInt(missed.kktCode); })[0].token;
    let kktRegNumber = tokens.filter(e => { return e.kkm_code == missed.kktCode; })[0].kktRegNumber;
    var g: Receipt[] = await ofg.grab(kktRegNumber, (new Date(Date.now() - 1000 * 5 * 60 * 60)).toISOString().substr(0, 10), (new Date(Date.now() )).toISOString().substr(0, 10), token);
    var missedArray = missed.outNumbers.split(' ').slice(1);
    missedArray.forEach(missedCheckNumber => {
      let checkdata = g.filter((receipt: Receipt) => {
        return receipt.requestNumber == Number.parseInt(missedCheckNumber);
      })[0];
      if (checkdata == undefined) {
        console.error(`check ${missedCheckNumber} has not in ofd. ib ${missed.ib}`);
      }
      else if (checkdata.cashTotalSum) {
        console.log(`!!! ${missed.ib}: ${JSON.stringify(checkdata)}`);
        result.push({ РМ: missed.host, 'Код кассы': missed.kktCode, 'Номер отсутствующего чека': checkdata.requestNumber, 'Сумма по данным ОФД': checkdata.cashTotalSum / 100, 'Кассир': checkdata.operator, ib: missed.ib, ib_href: missed.ib_href });
      }
    });
    //console.log(g)
  }
  catch (e) {
    console.error(e);
  }
}

function getTokens() {
  let q: String = `    
  select token, kktRegNumber, try_convert(int,kkm_code) kkm_code
  from
  dbo.tst_ofd_kkts k 
  inner join olap77.ref77_wrh.sup.ref_kkm kkm 
    on k.kktNumber = kkm.kkm_factory_number
    `

  let o: IOptions = { connectTimeout: 3900, maxRetriesOnTransientErrors: 1, "requestTimeout": 500000, "trustedConnection": false, enableArithAbort: false }
  let c: config = {
    database: 'OFD_storage',
    server: 'cube',
    connectionTimeout: 3900, user: 'sa', password: 'ser09l!', requestTimeout: 500000, options: o
  }
  let pool: mssql.ConnectionPool
  let conn: mssql.ConnectionPool
  var result: IResult<any>
  pool = new mssql.ConnectionPool(c)
  return Promise.resolve(pool.connect().then(
    conn => { return conn.query(q as any) }).then(
      result => { return result.recordset })


  )
}


